const { User } = require('../../models')
const { join } = require('path')
const bcrypt = require("bcrypt")

class HomeController {

    index = (req, res) => {
        User.findAll()
        .then(users => {
            res.render(join(__dirname, '../../views/index'), {
                content: './pages/userList',
                users: users
            })
        })
    }

    add = (req, res) => {
        res.render(join(__dirname, '../../views/index'), {
            content: './pages/addList'
        })
    }

    saveUser = async (req, res) => {
        const salt = await bcrypt.genSalt(10)

        User.create({
            name: req.body.name,
            username: req.body.username,
            age: req.body.age,
            password: await bcrypt.hash(req.body.password, salt)
        })
        .then(() => {
            res.redirect('/')
        }).catch(err => {
            console.log(err)
        })
    }
}

module.exports = HomeController
