const { join } = require('path')

class AuthController {

    login = (req,res) => {
        res.render(join(__dirname, '../../views/login'))
    }

    doLogin = (req,res) => {
        // is login success?
        res.render(join(__dirname, '../../views/login'))
    }

}

module.exports = AuthController