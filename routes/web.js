const { Router } = require('express')
const bodyParser = require('body-parser')

const AuthController = require('../controllers/web/AuthController')
const HomeController = require('../controllers/web/HomeController')

const web = Router()

web.use(bodyParser.json())
web.use(bodyParser.urlencoded({ extended: true }))

const authController = new AuthController
const homeController = new HomeController

web.get('/login', authController.login)
web.post('/login', authController.doLogin)

web.get('/add', homeController.add)
web.post('/save-user', homeController.saveUser)
web.get('/', homeController.index)

module.exports = web
